﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EventfulSearchApp;
using EventfulSearchApp.Controllers;
using System.Threading.Tasks;

namespace EventfulSearchApp.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.IsInstanceOfType(result.Model, typeof(Models.Search));
            Assert.IsNotNull(result.ViewBag);
            Assert.AreEqual("Search For Events", result.ViewBag.Title);
        }

        [TestMethod]
        public void SearchAsyncWithNullModel()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.SearchAsync(null).Result as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.Model);
        }

        [TestMethod]
        public void SearchAsyncWithNullResponse()
        {
            // Arrange
            HomeController controller = new HomeController();
            controller.Service = new TestEventfulService();

            // Act
            ViewResult result = controller.SearchAsync(new Models.Search()).Result as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);

            var resultModel = result.Model as Models.Search;
            Assert.IsNotNull(resultModel.Events);
            Assert.AreEqual(0, resultModel.Events.Count());
        }

        [TestMethod]
        public void SearchAsyncWithResponseException()
        {
            // Arrange
            HomeController controller = new HomeController();
            var exception = new Exception();
            controller.Service = new TestEventfulService() { ResponseException = exception };

            // Act
            ViewResult result = controller.SearchAsync(new Models.Search()).Result as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);

            var resultModel = result.Model as Models.Search;
            Assert.AreSame(exception, resultModel.Exception);
        }

        [TestMethod]
        public void SearchAsyncWithDefaultModel()
        {
            // Arrange
            HomeController controller = new HomeController();
            controller.Service = new TestEventfulService() { Response = new Models.EventfulResponse() };
            var model = new Models.Search();

            // Act
            ViewResult result = controller.SearchAsync(model).Result as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
            Assert.AreNotSame(model, result.Model, "Model Should be Recreated Upon a Successful Search");
            Assert.IsInstanceOfType(result.Model, typeof(Models.Search));

            var resultModel = result.Model as Models.Search;
            Assert.IsNotNull(resultModel.Events);
            Assert.AreEqual(string.Format("Found {0} Events", resultModel.Events.Count()), result.ViewBag.Title);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ViewBag);
            Assert.AreEqual("Interview Assignment for Colligo", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ViewBag);
            Assert.AreEqual("Created by Pat Sissons.", result.ViewBag.Message);
        }
    }

    public class TestEventfulService : IEventfulService
    {
        public Models.EventfulResponse Response { get; set; }

        public Exception ResponseException { get; set; }

        public Task<Models.EventfulResponse> GetEventfulResponseAsync(Models.Search model, int page = 1)
        {
            if (ResponseException != null)
            {
                throw ResponseException;
            }

            return Task.FromResult(Response);
        }
    }

}
