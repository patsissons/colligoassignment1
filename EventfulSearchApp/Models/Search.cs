﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EventfulSearchApp.Models
{
    public class Search
    {
        #region Fields

        public const double DefaultRadius = 1;
        public const EventCategory DefaultCategory = EventCategory.Music;

        public readonly DateTimeOffset DefaultStartDate = new DateTimeOffset(DateTime.Today);

        #endregion

        #region Constructors

        public Search()
            : this(null, null)
        {
        }

        public Search(Search model, IEnumerable<Event> events)
        {
            Events = events;

            ResetToDefaults();

            if (model != null)
            {
                Exception = model.Exception;
                Address = model.Address;
                LatLng = model.LatLng;
                Radius = model.Radius;
                StartDate = model.StartDate;
                EndDate = model.EndDate;
                Category = model.Category;
            }
        }

        #endregion

        #region Properties

        public IEnumerable<Event> Events { get; set; }

        public Exception Exception { get; set; }

        public string ErrorMessage
        {
            get
            {
#if DEBUG
                return Exception.ToString();
#else
                return Exception.Message;
#endif
            }
        }

        [Display(Name = "Address", Prompt = "Enter Full US Address")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string Address { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Geocoding Address Failed")]
        public string LatLng { get; set; }

        [Display(Name = "Radius", Description = "Kilometers", Prompt = "Enter Search Radius. i.e., 10.5")]
        [DataType(DataType.Text)]
        [Range(0, 300)] // the circumference of the earth is 40,075 KM, 300 KM is the spec requirement
        [RegularExpression(@"\d+(\.\d+)?", ErrorMessage = "Radius Must Be Numeric")]
        public double? Radius { get; set; }

        [Display(Name = "Start Date", Prompt = "dd/MM/yyyy")]
        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [ValidSearchDateRange]
        public DateTimeOffset? StartDate { get; set; }

        [Display(Name = "End Date", Prompt = "dd/MM/yyyy")]
        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [ValidSearchDateRange]
        public DateTimeOffset? EndDate { get; set; }

        [Display(Name = "Category")]
        [EnumDataType(typeof(EventCategory))]
        public EventCategory? Category { get; set; }

        public string EventfulCategory
        {
            get
            {
                string category = null;

                switch (Category ?? DefaultCategory)
                {
                    case EventCategory.PerformingArts:
                        category = "performing_arts";
                        break;
                    default:
                        category = Category.ToString().ToLowerInvariant();
                        break;
                }

                return category;
            }
        }

        #endregion

        #region Functions

        public void ResetToDefaults()
        {
            Address = string.Empty;
            Radius = DefaultRadius;
            StartDate = DefaultStartDate;
            EndDate = GetDefaultEndDate(StartDate.Value);
            Category = DefaultCategory;
        }

        public static DateTimeOffset GetDefaultEndDate(DateTimeOffset startDate)
        {
            return startDate.AddDays(1);
        }

        #endregion

        #region Sub-Types

        public enum EventCategory
        {
            [Display(Name = "Music")]
            Music,
            [Display(Name = "Sports")]
            Sports,
            [Display(Name = "Performing Arts")]
            PerformingArts
        }

        public class ValidSearchDateRangeAttribute : ValidationAttribute, IClientValidatable
        {
            public override bool IsValid(object value)
            {
                // always return true, validation is client side
                return true;
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                var rule = new ModelClientValidationRule()
                {
                    ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                    ValidationType = "validsearchdaterange"
                };

                rule.ValidationParameters.Add("datefield", metadata.PropertyName);

                yield return rule;
            }
        }


        #endregion
    }
}
