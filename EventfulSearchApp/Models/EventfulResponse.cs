﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventfulSearchApp.Models
{
    public class EventfulResponse
    {
        //public object last_item { get; set; }
        //public string total_items { get; set; }
        //public object first_item { get; set; }
        public int page_number { get; set; }
        //public string page_size { get; set; }
        //public object page_items { get; set; }
        //public string search_time { get; set; }
        public int page_count { get; set; }
        public Events events { get; set; }

        public void Merge(EventfulResponse response)
        {
            events.@event.AddRange(response.events.@event);
        }

        public class Performers
        {
            [JsonConverter(typeof(PerformerListConverter))]
            public List<Performer> performer { get; set; }
        }

        public class PerformerListConverter : JsonConverter
        {
            // only convert lists of performers (we never use this anyways)
            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(List<Performer>);
            }

            // eventful is kind of dumb and responds with a "schizophrenic" schema that projects performers 
            // as a list when there are multiple, and as a single object when there is only one
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                List<Performer> performers = null;

                if (reader.TokenType == JsonToken.StartArray)
                {
                    performers = serializer.Deserialize<List<Performer>>(reader);
                }
                else
                {
                    var performer = serializer.Deserialize<Performer>(reader);
                    performers = new List<Performer>(new[] { performer });
                }

                return performers;
            }

            // just serialize as a list (we never use this anyways)
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                serializer.Serialize(writer, value);
            }
        }

        public class Performer
        {
            //public string creator { get; set; }
            //public string linker { get; set; }
            public string name { get; set; }
            //public string url { get; set; }
            //public string id { get; set; }
            //public string short_bio { get; set; }
        }

        public class Image
        {
            //public string width { get; set; }
            public string url { get; set; }
            //public string height { get; set; }
        }

        public class ImageList
        {
            public Image small { get; set; }
            public Image medium { get; set; }
            public Image large { get; set; }
            public string caption { get; set; }
        }

        public class Event
        {
            //public object watching_count { get; set; }
            //public string olson_path { get; set; }
            //public object calendar_count { get; set; }
            //public object comment_count { get; set; }
            //public string region_abbr { get; set; }
            //public string postal_code { get; set; }
            //public object going_count { get; set; }
            //public string all_day { get; set; }
            public string latitude { get; set; }
            //public object groups { get; set; }
            //public string url { get; set; }
            public string id { get; set; }
            //public string privacy { get; set; }
            //public string city_name { get; set; }
            //public object link_count { get; set; }
            public string longitude { get; set; }
            //public string country_name { get; set; }
            //public string country_abbr { get; set; }
            //public string region_name { get; set; }
            public DateTimeOffset start_time { get; set; }
            //public object tz_id { get; set; }
            //public string description { get; set; }
            //public string modified { get; set; }
            //public string venue_display { get; set; }
            //public object tz_country { get; set; }
            public Performers performers { get; set; }
            public string title { get; set; }
            //public string venue_address { get; set; }
            //public string geocode_type { get; set; }
            //public object tz_olson_path { get; set; }
            //public string recur_string { get; set; }
            //public object calendars { get; set; }
            //public string owner { get; set; }
            //public object going { get; set; }
            //public string country_abbr2 { get; set; }
            public ImageList image { get; set; }
            //public string created { get; set; }
            //public string venue_id { get; set; }
            //public object tz_city { get; set; }
            //public object stop_time { get; set; }
            public string venue_name { get; set; }
            //public string venue_url { get; set; }

            public Uri ImageUri
            {
                get
                {
                    Uri uri = null;

                    if (image != null)
                    {
                        if (image.large != null)
                        {
                            return new Uri(image.large.url);
                        }
                        else if (image.medium != null)
                        {
                            return new Uri(image.medium.url);
                        }
                        else if (image.small != null)
                        {
                            return new Uri(image.small.url);
                        }
                    }

                    return uri;
                }
            }

            public string PerformerNames
            {
                get
                {
                    return (performers != null && performers.performer != null) ?
                        string.Join(", ", performers.performer.Select(x => x.name)) :
                        "No Performers to Display";
                }   
            }
        }

        public class Events
        {
            public List<Event> @event { get; set; }
        }
    }
}
