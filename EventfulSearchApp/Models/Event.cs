﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventfulSearchApp.Models
{
    public class Event
    {
        [Display(Name = "ID")]
        public string ID { get; set; }

        [Display(Name = "Image")]
        [DataType(DataType.ImageUrl)]
        [UIHint("ImageUri")]
        public Uri ImageUri { get; set; }

        [Display(Name = "Title")]
        [DataType(DataType.Text)]
        public string Title { get; set; }

        [Display(Name = "Venue")]
        [DataType(DataType.Text)]
        public string Venue { get; set; }

        [Display(Name = "Performers")]
        [DataType(DataType.Text)]
        public string Performers { get; set; }

        [Display(Name = "Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy, dddd}")]
        public DateTimeOffset Date { get; set; }
    }
}
