﻿using EventfulSearchApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventfulSearchApp.Controllers
{
    public interface IEventfulService
    {
        Task<EventfulResponse> GetEventfulResponseAsync(Search model, int page = 1);
    }
}
