﻿using EventfulSearchApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EventfulSearchApp.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            Service = EventfulService.Default;
        }

        public IEventfulService Service { get; set; }

        public ActionResult Index()
        {
            ViewBag.Title = "Search For Events";

            return View(new Search());
        }

        [AsyncTimeout(30000)]
        public async Task<ActionResult> SearchAsync(Search model)
        {
            if (model != null)
            {
                IEnumerable<Event> events = Enumerable.Empty<Event>();

                try
                {
                    //throw new Exception("test");

                    var response = await Service.GetEventfulResponseAsync(model);

                    if (response != null)
                    {
                        events = GetEvents(response)
                            .OrderBy(x => x.Date)
                            .ToList();
                    }
                }
                catch (Exception e)
                {
                    model.Exception = e;
                }

                model = new Search(model, events);

                ViewBag.Title = string.Format("Found {0} Events", model.Events.Count());
            }

            return View("Index",  model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Interview Assignment for Colligo";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Created by Pat Sissons.";

            return View();
        }

        public static IEnumerable<Event> GetEvents(EventfulResponse response)
        {
            return (response != null && response.events != null && response.events.@event != null) ?
                response.events.@event
                    .Select(x => new Event()
                    {
                        ID = x.id,
                        ImageUri = x.ImageUri,
                        Title = x.title,
                        Venue = x.venue_name,
                        Performers = x.PerformerNames,
                        Date = x.start_time
                    }) :
                Enumerable.Empty<Event>();
        }
    }
}