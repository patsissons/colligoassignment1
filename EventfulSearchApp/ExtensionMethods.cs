﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EventfulSearchApp
{
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Allows fetching model property metadata
        /// </summary>
        public static MvcHtmlString MetadataFor<TModel, TValue>(this HtmlHelper<TModel> source, Expression<Func<TModel, TValue>> expression, Func<ModelMetadata, object> selector)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, source.ViewData);
            return new MvcHtmlString(HttpUtility.HtmlEncode(selector(metadata)));
        }

        public static MvcHtmlString ImageFor<TModel>(this HtmlHelper<TModel> source, Func<TModel, object> srcSelector, object htmlAttributes)
        {
            var tag = new TagBuilder("img");
            var src = srcSelector(source.ViewData.Model);
            tag.Attributes.Add("src", src == null ? null : src.ToString());
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            return new MvcHtmlString(tag.ToString(TagRenderMode.SelfClosing));
        }
    }
}
